variable "repo_url" {
  type    = string
  default = "https://gitlab.com/wesjordansemailaddress/wesj-k3s-flux.git"
}

variable "repo_branch" {
  type    = string
  default = "master"
}

variable "target_path" {
  type    = string
  default = "flux"
}

variable "gitlab_username" {
    type = string
    default = "flux-v2"
}

variable "gitlab_password" {
    type = string
}